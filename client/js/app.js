/**
 * Created by instancetype on 9/17/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true, jquery : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(io) {
  'use strict';

  var socket = io()
    , scrollRegion = document.getElementById('scroll-region')
    , scroll
    , tweetContainerParent
    , handleNewTweet

  scroll = function scroll() {
    scrollRegion.scrollTop = scrollRegion.clientHeight
  }

  handleNewTweet = function handleNewTweet(data, options) {
    var constructHtml
      , createElements
      , appendElements
      , htmlStrings

    constructHtml = function constructHtml(data) {
      var username = data.username
        , userImg = '<img src=' + data.userImgUrl + ' />'
        , status = data.status
        , linkedName = '<a href="https://twitter.com/' + username.slice(1) + '" target="_blank">' + username + '</a>'
        , linkedImg = '<a href="https://twitter.com/' + username.slice(1) + '" target="_blank">' + userImg + '</a>'

      return [linkedName, linkedImg, status]
    }

    createElements = function createElements(htmlStrings, options) {
      return htmlStrings.map(function(htmlString, i) {
        var element = document.createElement(options.tweetPropContainerEl)
        element.innerHTML = htmlString

        switch (i) {
          case 0 :
            element.className = 'col-sm-3 name-data'
            break
          case 1 :
            element.className = 'col-sm-1 pic-data'
            break
          case 2 :
            element.className = 'col-sm-8 status-data'
            break
        }
        return element
      })
    }

    appendElements = function appendElements(elements, options) {
      var tweetContainer = document.createElement(options.tweetContainerEl)

      elements.forEach(function(element) {
        tweetContainer.appendChild(element)
      })
      options.tweetContainerParent.appendChild(tweetContainer)
    }

    appendElements( createElements( constructHtml(data), options), options)
  }


  tweetContainerParent = document.getElementById('tweet-table')
  socket.on('new tweet', function(tweet) {
    //console.log(tweet)

    var options = {
      tweetContainerParent : tweetContainerParent
    , tweetContainerEl : 'tr'
    , tweetPropContainerEl : 'td'
    }

    handleNewTweet(tweet, options)
    scroll()
  })

}(io))