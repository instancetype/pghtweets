/**
 * Created by instancetype on 9/17/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true, jquery : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
;(function STRICT(io) {
  'use strict';

  var socket = io()
    , scrollRegion = document.getElementById('scroll-region')
    , scroll
    , tweetContainerParent
    , handleNewTweet

  scroll = function scroll() {
    scrollRegion.scrollTop = scrollRegion.clientHeight
  }

  handleNewTweet = function handleNewTweet(data, options) {
    var constructHtml
      , createElements
      , appendElements
      , htmlStrings

    constructHtml = function constructHtml(data) {
      var username = data.username
        , userImg = '<img src=' + data.userImgUrl + ' />'
        , status = data.status
        , linkedName = '<a href="https://twitter.com/' + username.slice(1) + '" target="_blank">' + username + '</a>'
        , linkedImg = '<a href="https://twitter.com/' + username.slice(1) + '" target="_blank">' + userImg + '</a>'

      return [linkedName, linkedImg, status]
    }

    createElements = function createElements(htmlStrings, options) {
      return htmlStrings.map(function(htmlString, i) {
        var element = document.createElement(options.tweetPropContainerEl)
        element.innerHTML = htmlString

        switch (i) {
          case 0 :
            element.className = 'col-sm-3 name-data'
            break
          case 1 :
            element.className = 'col-sm-1 pic-data'
            break
          case 2 :
            element.className = 'col-sm-8 status-data'
            break
        }
        return element
      })
    }

    appendElements = function appendElements(elements, options) {
      var tweetContainer = document.createElement(options.tweetContainerEl)

      elements.forEach(function(element) {
        tweetContainer.appendChild(element)
      })
      options.tweetContainerParent.appendChild(tweetContainer)
    }

    appendElements( createElements( constructHtml(data), options), options)
  }


  tweetContainerParent = document.getElementById('tweet-table')
  socket.on('new tweet', function(tweet) {
    //console.log(tweet)

    var options = {
      tweetContainerParent : tweetContainerParent
    , tweetContainerEl : 'tr'
    , tweetPropContainerEl : 'td'
    }

    handleNewTweet(tweet, options)
    scroll()
  })

}(io))
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJjb21iaW5lZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ3JlYXRlZCBieSBpbnN0YW5jZXR5cGUgb24gOS8xNy8xNC5cbiAqL1xuIC8qIGpzaGludCBiaXR3aXNlIDogdHJ1ZSwgZXFlcWVxIDogdHJ1ZSwgZm9yaW4gOiB0cnVlLCBub2FyZyA6IHRydWUsIG5vZW1wdHkgOiB0cnVlLCBub25ldyA6IHRydWUsIGpxdWVyeSA6IHRydWUsXG4gICBhc2kgOiB0cnVlLCBlc25leHQgOiB0cnVlLCBsYXhjb21tYSA6IHRydWUsIHN1YiA6IHRydWUsIGJyb3dzZXIgOiB0cnVlLCBub2RlIDogdHJ1ZSwgcGhhbnRvbSA6IHRydWUgKi9cbjsoZnVuY3Rpb24gU1RSSUNUKGlvKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICB2YXIgc29ja2V0ID0gaW8oKVxuICAgICwgc2Nyb2xsUmVnaW9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Njcm9sbC1yZWdpb24nKVxuICAgICwgc2Nyb2xsXG4gICAgLCB0d2VldENvbnRhaW5lclBhcmVudFxuICAgICwgaGFuZGxlTmV3VHdlZXRcblxuICBzY3JvbGwgPSBmdW5jdGlvbiBzY3JvbGwoKSB7XG4gICAgc2Nyb2xsUmVnaW9uLnNjcm9sbFRvcCA9IHNjcm9sbFJlZ2lvbi5jbGllbnRIZWlnaHRcbiAgfVxuXG4gIGhhbmRsZU5ld1R3ZWV0ID0gZnVuY3Rpb24gaGFuZGxlTmV3VHdlZXQoZGF0YSwgb3B0aW9ucykge1xuICAgIHZhciBjb25zdHJ1Y3RIdG1sXG4gICAgICAsIGNyZWF0ZUVsZW1lbnRzXG4gICAgICAsIGFwcGVuZEVsZW1lbnRzXG4gICAgICAsIGh0bWxTdHJpbmdzXG5cbiAgICBjb25zdHJ1Y3RIdG1sID0gZnVuY3Rpb24gY29uc3RydWN0SHRtbChkYXRhKSB7XG4gICAgICB2YXIgdXNlcm5hbWUgPSBkYXRhLnVzZXJuYW1lXG4gICAgICAgICwgdXNlckltZyA9ICc8aW1nIHNyYz0nICsgZGF0YS51c2VySW1nVXJsICsgJyAvPidcbiAgICAgICAgLCBzdGF0dXMgPSBkYXRhLnN0YXR1c1xuICAgICAgICAsIGxpbmtlZE5hbWUgPSAnPGEgaHJlZj1cImh0dHBzOi8vdHdpdHRlci5jb20vJyArIHVzZXJuYW1lLnNsaWNlKDEpICsgJ1wiIHRhcmdldD1cIl9ibGFua1wiPicgKyB1c2VybmFtZSArICc8L2E+J1xuICAgICAgICAsIGxpbmtlZEltZyA9ICc8YSBocmVmPVwiaHR0cHM6Ly90d2l0dGVyLmNvbS8nICsgdXNlcm5hbWUuc2xpY2UoMSkgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCI+JyArIHVzZXJJbWcgKyAnPC9hPidcblxuICAgICAgcmV0dXJuIFtsaW5rZWROYW1lLCBsaW5rZWRJbWcsIHN0YXR1c11cbiAgICB9XG5cbiAgICBjcmVhdGVFbGVtZW50cyA9IGZ1bmN0aW9uIGNyZWF0ZUVsZW1lbnRzKGh0bWxTdHJpbmdzLCBvcHRpb25zKSB7XG4gICAgICByZXR1cm4gaHRtbFN0cmluZ3MubWFwKGZ1bmN0aW9uKGh0bWxTdHJpbmcsIGkpIHtcbiAgICAgICAgdmFyIGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KG9wdGlvbnMudHdlZXRQcm9wQ29udGFpbmVyRWwpXG4gICAgICAgIGVsZW1lbnQuaW5uZXJIVE1MID0gaHRtbFN0cmluZ1xuXG4gICAgICAgIHN3aXRjaCAoaSkge1xuICAgICAgICAgIGNhc2UgMCA6XG4gICAgICAgICAgICBlbGVtZW50LmNsYXNzTmFtZSA9ICdjb2wtc20tMyBuYW1lLWRhdGEnXG4gICAgICAgICAgICBicmVha1xuICAgICAgICAgIGNhc2UgMSA6XG4gICAgICAgICAgICBlbGVtZW50LmNsYXNzTmFtZSA9ICdjb2wtc20tMSBwaWMtZGF0YSdcbiAgICAgICAgICAgIGJyZWFrXG4gICAgICAgICAgY2FzZSAyIDpcbiAgICAgICAgICAgIGVsZW1lbnQuY2xhc3NOYW1lID0gJ2NvbC1zbS04IHN0YXR1cy1kYXRhJ1xuICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZWxlbWVudFxuICAgICAgfSlcbiAgICB9XG5cbiAgICBhcHBlbmRFbGVtZW50cyA9IGZ1bmN0aW9uIGFwcGVuZEVsZW1lbnRzKGVsZW1lbnRzLCBvcHRpb25zKSB7XG4gICAgICB2YXIgdHdlZXRDb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KG9wdGlvbnMudHdlZXRDb250YWluZXJFbClcblxuICAgICAgZWxlbWVudHMuZm9yRWFjaChmdW5jdGlvbihlbGVtZW50KSB7XG4gICAgICAgIHR3ZWV0Q29udGFpbmVyLmFwcGVuZENoaWxkKGVsZW1lbnQpXG4gICAgICB9KVxuICAgICAgb3B0aW9ucy50d2VldENvbnRhaW5lclBhcmVudC5hcHBlbmRDaGlsZCh0d2VldENvbnRhaW5lcilcbiAgICB9XG5cbiAgICBhcHBlbmRFbGVtZW50cyggY3JlYXRlRWxlbWVudHMoIGNvbnN0cnVjdEh0bWwoZGF0YSksIG9wdGlvbnMpLCBvcHRpb25zKVxuICB9XG5cblxuICB0d2VldENvbnRhaW5lclBhcmVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0d2VldC10YWJsZScpXG4gIHNvY2tldC5vbignbmV3IHR3ZWV0JywgZnVuY3Rpb24odHdlZXQpIHtcbiAgICAvL2NvbnNvbGUubG9nKHR3ZWV0KVxuXG4gICAgdmFyIG9wdGlvbnMgPSB7XG4gICAgICB0d2VldENvbnRhaW5lclBhcmVudCA6IHR3ZWV0Q29udGFpbmVyUGFyZW50XG4gICAgLCB0d2VldENvbnRhaW5lckVsIDogJ3RyJ1xuICAgICwgdHdlZXRQcm9wQ29udGFpbmVyRWwgOiAndGQnXG4gICAgfVxuXG4gICAgaGFuZGxlTmV3VHdlZXQodHdlZXQsIG9wdGlvbnMpXG4gICAgc2Nyb2xsKClcbiAgfSlcblxufShpbykpIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9