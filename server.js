/**
 * Created by instancetype on 11/26/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true, devel : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
'use strict';

const
  express = require('express')
, app = express()
, server = require('http').Server(app)
, io = require('socket.io')(server)
, Twit = require('twit')
, morgan = require('morgan')

const
  PORT = process.env.PORT || 3000
, Path = require('path')

if (!(process.env.heroku)) {
  var config = require('./config.js')
}

app.use(morgan('dev'))
app.use(express.static(Path.join(__dirname, 'client', 'dist')))

app.get('/', function(req, res) {
  res.sendFile('index.html', { root : Path.join(__dirname, 'client', 'dist') })
})


server.listen(PORT, function() {
  console.log('Server listening on port', PORT)
})

var twit
  , stream
  , pittsburghArea

twit = new Twit(
  { consumer_key : process.env.consumer_key || config.twitter.consumer_key
  , consumer_secret : process.env.consumer_secret || config.twitter.consumer_secret
  , access_token : process.env.access_token || config.twitter.access_token
  , access_token_secret : process.env.access_token_secret || config.twitter.access_token_secret
  }
)

pittsburghArea = ['-80.284105', '40.266804', '-79.776803', '40.651109']
stream = twit.stream('statuses/filter', { locations : pittsburghArea })

io.on('connection', function(socket) {

  stream.on('tweet', function(tweet) {
    //console.log(tweet)

    var username = '@' + tweet.user.screen_name
      , userImgUrl = tweet.user.profile_image_url
      , status = tweet.text

    socket.emit('new tweet', { username : username, status : status, userImgUrl : userImgUrl })
  })
})