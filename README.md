PghTweets - A quick streaming demo
===========
- Express
- Socket.IO
- Twitter API
- Bootstrap
- Less
- Gulp

## About

Function composition? Yes.
Separation of concerns? Not so much.

Just passing the time experimenting with streaming data from twitter. It's only styled enough to be readable.


## Status
Pretty bare-bones but functional for what it is... I may well do something more with this, because it's kind of cool
and fun to work with.

You can see it in its current condition at [PghTweets](http://pghtweets.instancetype.io).