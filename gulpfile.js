/**
 * Created by instancetype on 9/16/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  gulp = require('gulp')
, less = require('gulp-less')
, sourcemaps = require('gulp-sourcemaps')
, concatCSS = require('gulp-concat-css')
, minifyCSS = require('gulp-minify-css')
, rename = require('gulp-rename')
, jshint = require('gulp-jshint')
, stylish = require('jshint-stylish')
, uglify = require('gulp-uglify')
, concat = require('gulp-concat')

const
  SCRIPTS = ['./client/js/app.js']

gulp.task('less', function() {
  return gulp.src('./client/styles/less/*.less')
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./client/styles/css'))
})

gulp.task('concat-css', ['less'], function() {
  return gulp.src(
    [ './client/vendor/bootstrap/dist/css/bootstrap.min.css'
      , './client/styles/css/style.css'
    ]
  ).pipe(concatCSS('combined.css'))
    .pipe(gulp.dest('./client/styles/css'))
})

gulp.task('minify-css', ['concat-css'], function() {
  return gulp.src('./client/styles/css/combined.css')
    .pipe(minifyCSS())
    .pipe(rename(function(path) {
      path.basename += '.min'
    }))
    .pipe(gulp.dest('./client/dist'))
})

gulp.task('lint', function() {
  return gulp.src(['*.js', './models/*.js', './client/js/app.js'])
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
})

gulp.task('concat-js', ['lint'], function() {
  return gulp.src(SCRIPTS)
    .pipe(sourcemaps.init())
    .pipe(concat('combined.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./client/js'))
})

gulp.task('minify-js', ['concat-js'], function() {
  return gulp.src('./client/js/combined.js')
    .pipe(uglify())
    .pipe(rename(function(path) {
      path.basename += '.min'
    }))
    .pipe(gulp.dest('./client/dist'))
})

gulp.task('watch', function() {
  gulp.watch('./client/styles/less/*.less', ['minify-css'])
})

gulp.task('finalize', ['minify-css', 'minify-js'])